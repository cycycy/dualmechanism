#include "MinCostMaxFlow.h"
#include <iostream>
#include <vector>
#include <map>
#include <queue>

using namespace std;

int
MinCostMaxFlow::addVertex()
{
	int res = graph.size();
	map<int,pair<int,double> > neigh;
	graph.push_back(neigh);
	return res;
}

void
MinCostMaxFlow::addEdge(int u, int v, int cap, double cost)
{
	graph[u][v] = pair<int,double>(cap, cost);
}

double
MinCostMaxFlow::minCostMaxFlow(int source, int sink)
{
	double result = 0, cost;
	while (SPFA(source, sink, cost)) result += cost;
	return result;
}

int
MinCostMaxFlow::SPFA(int source, int sink, double &result)
{
	vector<int> visit(graph.size(), 0), flow(graph.size(), 0), path(graph.size(), 0);
    vector<double> dist(graph.size(), 0);
	queue<int> q;

	visit[source] = 1;
	flow[source] = path[source] = -1;
	q.push(source);
	while (!q.empty())
	{
        int curr=q.front();
		for (map<int,pair<int,double> >::iterator itr = graph[curr].begin(); itr != graph[curr].end(); itr++)
		{
			int neigh = itr->first, cap = (itr->second).first;
            double cost = (itr->second).second;
			if (cap > 0 && (!visit[neigh] || dist[curr] + cost < dist[neigh]-1e-5))
			{
				visit[neigh] = 1;
				dist[neigh] = dist[curr] + cost;
				flow[neigh] = flow[curr] < 0? cap: min(cap, flow[curr]);
				path[neigh] = curr;
				q.push(neigh);
			}
		}
		q.pop();
	}

	if (!visit[sink]) return 0;
	result = dist[sink] * flow[sink];
	int curr = sink;
	while (path[curr] >= 0)
	{
		graph[path[curr]][curr].first -= flow[sink];
		graph[curr][path[curr]].first += flow[sink];
		graph[curr][path[curr]].second = -graph[path[curr]][curr].second;
		curr = path[curr];
	}
	return 1;
}

int
MinCostMaxFlow::getCap(int u, int v)
{
	return graph[u][v].first;
}
