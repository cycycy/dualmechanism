#include "VirtualVCG.h"
#include "NLF.h" 
#include "BoundConstraint.h"
#include "LinearInequality.h"
#include "NonLinearInequality.h"
#include "CompoundConstraint.h"
#include "OptNIPS.h"
#include "OptNPSOL.h"
#include "DualMechanism.h"
#include "OptQNewton.h"

using NEWMAT::ColumnVector;
using NEWMAT::Matrix;
using NEWMAT::SymmetricMatrix; 
using namespace OPTPP;

    void 
DualMechanism::init_fx(int ndim, ColumnVector& x)
{
    if (ndim != M*T*T)
        exit(1);
    for (int i=0;i<M*T*T;i++) x(i+1)=0;
}
    void 
DualMechanism::fx(int ndim, const ColumnVector& x, double& fx, 
        ColumnVector& gx)
{
    if (ndim != M*T*(T))
        exit(1);
    double sum = F[0];
    for (int j=0;j<T;j++)
    {
        sum-=x(Index(0,0,j));
        sum+=x(Index(0,j,0));
    }
//    std::cout<<sum<<' ';
    double * w=new double[M*T*N];
    for (int i=0;i<M;i++)
        for (int j=0;j<T;j++)
            for (int k=0;k<N;k++)
            {
                w[Convert(i,j,k)]=type[Convert(i,j,k)];
                double sum = 0;
                for (int jj=0;jj<T;jj++)
                    sum += x(Index(i,jj,j))*(type[Convert(i,jj,k)]-type[Convert(i,j,k)]);
                sum = sum / F[Convert2(i,j)];
                w[Convert(i,j,k)]-=sum;
            }
/*    for (int i=0;i<M*T*N;i++)
        std::cout<<w[i]<<' ';
    std::cout<<endl;*/
/*    for (int i=0;i<ndim;i++)
        std::cout<<x(i+1)<<' ';
    std::cout<<endl;*/
    VirtualVCG* vcg = new VirtualVCG(w,F,M,T,N);
    vcg -> SolveVCG();
//    std::cout<<vcg -> GetRevenue()<<endl;
    /*    for (int i=0; i<M;i++)
          {
          for (int j=0;j<T;j++)
          {
          for (int k=0;k<M;k++)
          std::cout<<vcg -> GetReducedForm(i,j,k)<<' ';
          std::cout<<endl; }
          std::cout<<endl;
          }*/
    fx = vcg -> GetRevenue();
//    std::cout<<"haha"<<endl;
    for (int i=0;i<M;i++)
        for (int j=0;j<T;j++) {
            for (int jj=0;jj<T;jj++)
            {
                int ind = Index(i,j,jj);
                double sum = 0;
                for (int k=0;k<N;k++)
                    sum += (type[Convert(i,jj,k)]-type[Convert(i,j,k)])*(vcg->GetReducedForm(i,jj,k));
                gx(ind) = sum;
//                std::cout<<sum<<' ';
            }
        }
//    std::cout<<endl;
    delete vcg;
    delete w;
}


    int
DualMechanism::Convert(int m,int t,int n)
{
    return m*T*N+t*N+n;
}

    int
DualMechanism::Convert2(int m,int t)
{
    return m*T+t;
}
    int
DualMechanism::Index(int i,int t1,int t2)
{
    return i*T*T+t1*T+t2+1;
}

DualMechanism::DualMechanism(int m,int t,int n)
{
    M=m;
    T=t;
    N=n;
    Rev=0;
    F=new double[M*T];
    type=new double[M*T*N];
}

    bool
DualMechanism::SetType(double *** v)
{
    for (int i=0;i<M;i++)
        for (int j=0;j<T;j++)
            for (int k=0;k<N;k++)
                type[Convert(i,j,k)]=v[i][j][k];
    return true;
}

    bool
DualMechanism::SetDistribution(double ** f)
{
    for (int i=0;i<M;i++)
        for (int j=0;j<T;j++)
            F[Convert2(i,j)]=f[i][j];
    return true;
}

    bool
DualMechanism::Solve(int iter,double a)
{
    int ndim=M*T*T;
    //    std::cout<<M<<' '<<T<<' '<<N<<' '<<ndim<<endl;
    ColumnVector lower(ndim); //bound constraint
    for (int i=0;i<ndim;i++)
        lower(i+1)=0;
    Constraint c1 = new BoundConstraint(ndim,lower);
    //flow constraint
    Matrix cons(M*T,ndim);
    ColumnVector consrhs(M*T);
    for (int i=0;i<M;i++)
        for (int j=0;j<T;j++)
        {
            int num=Convert2(i,j);
            consrhs(num+1)=F[num]; 
            for (int jj=0;jj<T;jj++)
            {
                //		std::cout<<i<<' '<<j<<' '<<jj<<' '<<Index(i,j,jj)<<endl;
                cons(num+1,Index(i,j,jj))=1;
                cons(num+1,Index(i,jj,j))=-1;
                if (j == jj) cons(num+1,Index(i,j,jj)) = 0;
            }
        }
    double f;
    ColumnVector g(ndim);
    ColumnVector x(ndim);
    init_fx(ndim,x);
    fx(ndim,x,f,g); 
    double min = f;
    double prev = f;
    int k = 0;
    for (int it=0;it<iter;it++)
    {
        bool bo=true;
        for (int num=1; num <= M*T; num++)
        {
            double sum = 0;
            for (int i = 0; i< ndim; i++)
                sum+=x(i+1)*cons(num,i+1);
            if (sum > consrhs(num))
            {
                bo=false;
                for (int i=0;i<ndim;i++)
                    x(i+1)=x(i+1)-a*cons(num,i+1);
            }
        }
        if (bo)
        {
            fx(ndim,x,f,g);
            for (int i=0;i<ndim;i++)
            {
                x(i+1)=x(i+1)-a*g(i+1);
                if (x(i+1)<0) x(i+1)=0;
            }
            if (min>f) 
            {
                min = f;
                k=0;
            }
            else
            {
                k++;
                if (k > 1000)
                {
                    a/=2;
                    if (a < 1e-7)
                        a*=2;
                    k=0;
                }
            }
        }
    }
    Rev=min;    
    return true;
}

    double
DualMechanism::GetExpectedRevenue()
{
    return Rev;
}

int DualMechanism::M;
int DualMechanism::T;
int DualMechanism::N;
double *DualMechanism::F;
double *DualMechanism::type;
double DualMechanism::Rev;

