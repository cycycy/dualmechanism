#ifndef DUAL_MECH_H
#define DUAL_MECH_H
#include "VirtualVCG.h"
#include "NLF.h"
#include "BoundConstraint.h"
#include "LinearEquation.h"
#include "NonLinearInequality.h"
#include "CompoundConstraint.h"
#include "OptNIPS.h"

using NEWMAT::ColumnVector;
using NEWMAT::Matrix;
using NEWMAT::SymmetricMatrix;

using namespace OPTPP;
class DualMechanism
{
    private:

        /* Number of Bidders, Number of Types, Number of Objects*/ static int M, T, N; /* Bidder's Types of size (M * T * N) */
        static double *type;
        /* Distributions of Bidder's Valuations of size (M * T) */
        static double *F;
        /* Expected Revenue */
        static double Rev;
        static int Convert(int, int, int);
        static int Convert2(int, int);
        static int Index(int, int, int);
        static void init_fx(int ndim, ColumnVector& x);
        static void fx(int ndim, const ColumnVector& x, double& fx, 
                ColumnVector& gx);




    public:

        DualMechanism(int, int, int);//M,T,N

        bool SetType(double***);
        bool SetDistribution(double**);
        bool Solve(int ,double);
        double GetExpectedRevenue();
};
#endif

