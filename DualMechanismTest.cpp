#include "DualMechanism.h"

#include <cstdio>
#include <map>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

const int ITEM_NUMBER = 1;
const int BIDDER_NUMBER = 2;
const int TYPE_NUMBER = 2;

int main()
{
    int n=ITEM_NUMBER,m=BIDDER_NUMBER,t=TYPE_NUMBER;
    double ***type= new double**[m];
    for (int i=0;i<m;i++)
    {
        type[i]=new double*[t];
        for (int j=0;j<t;j++)
            type[i][j]=new double[n];
    }
    double **distribution= new double*[m];
    for (int i=0;i<m;i++)
        distribution[i]=new double[t];
    for (int i=0;i<m;i++)
        for (int j=0;j<t;j++)
            for (int k=0;k<n;k++)
                type[i][j][k]=j*10+i+5;
    for (int i=0;i<m;i++)
        for (int j=0;j<t;j++)
            distribution[i][j]=1.0/t;
    DualMechanism mech(m,t,n);
    mech.SetType(type);
    mech.SetDistribution(distribution);
    mech.Solve(1e5,1e-4);
    cout<<mech.GetExpectedRevenue()<<endl;
    return 0;
}

