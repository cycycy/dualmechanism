CPP = g++
CFLAGS = -Wall -O3

all: VirtualVCGTest DualMechanismTest Run

Run: DualMechanism DualMechanism.cpp
	$(CPP) $(CFLAGS) -DHAVE_STD -DHAVE_NAMESPACES Run.cpp *.o -o Run -lopt -lnewmat

DualMechanismTest: DualMechanism DualMechanism.cpp
	$(CPP) $(CFLAGS) -DHAVE_STD -DHAVE_NAMESPACES DualMechanismTest.cpp *.o -o DualMechanismTest -lopt -lnewmat

DualMechanism: VirtualVCG DualMechanism.h DualMechanism.cpp 
	$(CPP) $(CFLAGS) -c -DHAVE_NAMESPACES DualMechanism.cpp -lopt -lnewmat

VirtualVCGTest: VirtualVCG VirtualVCGTest.cpp
	$(CPP) $(CFLAGS) VirtualVCGTest.cpp *.o -o VirtualVCGTest -lopt -lnewmat


VirtualVCG: MinCostMaxFlow VirtualVCG.h VirtualVCG.cpp
	$(CPP) $(CFLAGS) -c VirtualVCG.cpp

MinCostMaxFlow: MinCostMaxFlow.h MinCostMaxFlow.cpp
	$(CPP) $(CFLAGS) -c MinCostMaxFlow.cpp


clean:
	rm -f *~ *.o VirtualVCGTest DualMechanismTest log OPT_DEFAULT.out Run
