#include "VirtualVCG.h"
#include <iostream>
using namespace std;

VirtualVCG::VirtualVCG(double* w,double *D,int M,int T,int N)
{
    this->M = M;
    this->T = T;
    this->N = N;

    this->w = w;
    this->D = D;

    index = new int[M];
    advertiser = new int[M];
    slot = new int[N];
    reducedform = new double[M*T*N];
}

VirtualVCG::~VirtualVCG()
{
    delete [] index;
    delete [] advertiser;
    delete [] slot;
    delete [] reducedform;
}

    void
VirtualVCG::dfs(int depth)
{
    if (depth == M)
    {
        VCGAllocation();
        return;
    }
    for (int i = 0; i < T; i++)
    {
        if (D[depth*T+i] < 1e-5) continue;
        index[depth] = i;
        dfs(depth+1);
    }
}

    void
VirtualVCG::VCGAllocation()
{
    /* no constraint */
    for (int j=0;j<N;j++)
    {
        int allocate=-1;
        double max=0;
        for (int i=0;i<M;i++)
        {
            if (w[i*T*N+index[i]*N+j] > max+1e-5)
            {
                allocate=i;
                max=w[i*T*N+index[i]*N+j]; 
            }
        }
        if (allocate==-1) return;
        int i=allocate;
        double prob=1;
        for (int k=0;k<M;k++)
            if (k != i)
                prob*=D[k*T+index[k]];
        reducedform[i*T*N+index[i]*N+j] += prob;
    }
    
    /* one object to one bidder */
 /*         network = new MinCostMaxFlow();

          int source = network->addVertex(), sink = network->addVertex();
          for (int i = 0; i < M; i++) advertiser[i] = network->addVertex();
          for (int i = 0; i < N; i++) slot[i] = network->addVertex();

          for (int i = 0; i < M; i++) network->addEdge(source, advertiser[i], 1, 0);
          for (int i = 0; i < N; i++) network->addEdge(slot[i], sink, 1, 0);
          for (int i = 0; i < M; i++)
          for (int j = 0; j < N; j++)
          if (w[i*T*N+index[i]*N+j] > 0)
            network->addEdge(advertiser[i], slot[j], 1, -w[i*T*N+index[i]*N+j]);
          else
            network->addEdge(advertiser[i], slot[j], 1, 0);

          network->minCostMaxFlow(source, sink) ;

          for (int i = 0; i < M; i++)
          for (int j = 0; j < N; j++)
          if (w[i*T*N+index[i]*N+j] > 0)
          {
          double prob = 1;
          for (int k = 0; k < M; k++)
          if (i != k) prob *= D[k*T+index[k]];
          reducedform[i*T*N+index[i]*N+j] += prob * (network->getCap(advertiser[i], slot[j])? 0: 1);
          }
          delete network;*/
          
}

    void
VirtualVCG::SolveVCG()
{
    for (int i = 0; i < M; i++)
        for (int j = 0; j < T; j++)
            for (int k = 0; k < N; k++)
                reducedform[i*T*N+j*N+k] = 0;
    /* one object per bidder */
    dfs(0);
    /* additive */
    /*
    for (int i=0;i<M;i++)
        for (int j=0;j<T;j++)
            for (int k=0;k<N;k++)
            {
                if (D[i*T+j]<1e-5 || w[i*T*N+j*N+k]<1e-5)
                    continue;
                reducedform[i*T*N+j*N+k]=1;
                for (int ii=0;ii<M;ii++)
                {
                    double prob=0;
                    for (int jj=0;jj<T;jj++)
                    {
                        if (i<ii)
                            if (w[i*T*N+j*N+k]/D[i*T+j]>w[ii*T*N+jj*N+k]/D[ii*T+jj]-1e-5)
                                prob+=D[ii*T+jj];
                        if (i>ii)
                            if (w[i*T*N+j*N+k]/D[i*T+j]>w[ii*T*N+jj*N+k]/D[ii*T*jj]+1e-5)
                                prob+=D[ii*T+jj];
                    }
                    if (i!=ii) reducedform[i*T*N+j*N+k]*=prob;
                }
            }                
            */
}

    double
VirtualVCG::GetReducedForm(int m,int t,int n)
{
    return reducedform[m*T*N+t*N+n];
}
    double
VirtualVCG::GetRevenue()
{
    double result=0;
    for (int i=0;i<M;i++)
        for (int j=0;j<T;j++)
            for (int k=0;k<N;k++)
                result+=reducedform[i*T*N+j*N+k]*D[i*T+j]*w[i*T*N+j*N+k];
    return result;
}
