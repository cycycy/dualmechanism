#ifndef MINCOSTMAXFLOW_H
#define MINCOSTMAXFLOW_H
#include <vector>
#include <map>

using namespace std;

class MinCostMaxFlow
{
public:
	int addVertex();
	void addEdge(int, int, int, double);
	double minCostMaxFlow(int, int);
	int getCap(int, int);

private:
	int SPFA(int, int, double&);

	vector<map<int,pair<int,double> > > graph;
};
#endif
