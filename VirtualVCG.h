#ifndef VVCG_H
#define VVCG_H
#include "MinCostMaxFlow.h"
class VirtualVCG
{
    private:
        /* Number of Bidders, Number of Types, Number of Objects */
        int M, T, N;
        /* Virtual VCG Weights of size (M * T * N) */
        double *w;
        /* Distributions of Advertiser's Valuations of size (M * T) */
        double *D;

        /* Type Index of Advertiser's Valuations of size M */
        int *index;
        /* Advertiser Index in MinCostMaxFlow Network of size M */
        int *advertiser;
        /* Slot Index in MinCostMaxFlow Network of size N */
        int *slot;
        /* Reduced Form of Advertisers of size(M * T * N) */
        double *reducedform;

        /* MinCostMaxFlow Algorithm for Maximizxing Social Welfare */
        MinCostMaxFlow *network;

        /* Enumerate Different Types of Different Advertisers */
        void dfs(int);
        /* Virtual VCG Allocation Maximizing Social Welfare */
        void VCGAllocation();

    public:
        VirtualVCG(double*,double*,int,int,int);
        ~VirtualVCG();

        void SolveVCG();
        double GetReducedForm(int,int,int);
        double GetRevenue();
};


#endif
