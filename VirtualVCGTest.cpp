#include"VirtualVCG.h"
#include<iostream>
using namespace std;
int main()
{
    int m = 3, t = 2, n = 2;
    double *distribution = new double[m*t];
    for (int i=0;i<m*t;i++)
        distribution[i]=1.0/t;
    double *w = new double[m*t*n];
    w[0]=0.1;
    w[1]=0.2;
    w[2]=0.5;
    w[3]=0.6;
    w[4]=0.2;
    w[5]=0.5;
    w[6]=0.5;
    w[7]=0.2;
    w[8]=0.3;
    w[9]=0.4;
    w[10]=0.4;
    w[11]=0.3;
    VirtualVCG *vcg=new VirtualVCG(w,distribution,m,t,n);
    vcg->SolveVCG();
    for (int i=0;i<m;i++)
        for (int j=0;j<t;j++)
        {
            for (int k=0;k<n;k++)
                cout<<vcg->GetReducedForm(i,j,k)<<' ';
            cout<<endl;
        }
    cout<<vcg->GetRevenue()<<endl;
    delete vcg;
    return 0;
}


