#include <cstdio>
#include <map>
#include <algorithm>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

const int ITEM_NUMBER = 1;
const int BIDDER_NUMBER = 1;
const int TYPE_NUMBER = 3; 
int valuation[BIDDER_NUMBER][ITEM_NUMBER][TYPE_NUMBER];

double PL[5] = {0.7752,0.1370,0.0497,0.0242,0.0139};
double ans = 0;
double maxi[ITEM_NUMBER];

double bid[BIDDER_NUMBER][TYPE_NUMBER];
double v_bid[BIDDER_NUMBER][TYPE_NUMBER];

double dis[BIDDER_NUMBER][TYPE_NUMBER];
double F[BIDDER_NUMBER][TYPE_NUMBER];

double Myerson()
{
    int n=BIDDER_NUMBER,t=TYPE_NUMBER;
    for (int i=0;i<n;i++)
        for (int j=0;j<t;j++)
        {
            if (j==0)
                F[i][j]=dis[i][j];
            else
                F[i][j]=F[i][j-1]+dis[i][j];
            if (j==t-1)
                v_bid[i][j]=bid[i][j];
            else
                v_bid[i][j]=bid[i][j]-(bid[i][j+1]-bid[i][j])*(1-F[i][j])/dis[i][j];
        }
    for (int i=0;i<n;i++)
        for (int j=0;j<t;j++)
        {
            int jj=j-1;
            while (jj >= 0 && v_bid[i][j] < v_bid[i][jj])
            {
                double k = v_bid[i][j]*(F[i][j]-F[i][jj])+v_bid[i][jj]*dis[i][jj];
                if (jj == 0)
                    k /= F[i][j];
                else
                    k /= F[i][j]-F[i][jj-1];
                jj--;
            }
        }
    double rev = 0;
    for (int i=0;i<n;i++)
        for (int j=0;j<t;j++)
        {
            if (v_bid[i][j]<1e-5) continue;
            double q = 1;
            for (int ii=0;ii<n;ii++)
                if (ii != i)
                {
                    double prob = 0;
                    for (int jj=0;jj<t;jj++)
                    {
                        if (ii > i && v_bid[i][j] > v_bid[ii][jj] - 1e-5)
                            prob += dis[ii][jj];
                        if (ii < i && v_bid[i][j] > v_bid[ii][jj] + 1e-5)
                            prob += dis[ii][jj];
                    }
                    q *= prob;
                }
            rev += v_bid[i][j]*q*dis[i][j];
        }
    return rev;
}

int main()
{
    int n=ITEM_NUMBER,m=BIDDER_NUMBER,tt=TYPE_NUMBER;
    int t=1;
    for (int i=0;i<n;i++)
        t*=tt;
    cout<<t<<endl;
    double **distribution= new double*[m];
    for (int i=0;i<m;i++)
        distribution[i]=new double[t];
    for (int i=0;i<m;i++)
        for (int k=0; k<n; k++)
            for (int j=0; j<tt;j++)
                scanf("%d ", &valuation[i][k][j]);
/*    for (int i=0;i<m;i++)
    {
        for (int k=0;k<n;k++)
        {
            for (int j=0;j<tt;j++)
                cout<<valuation[i][k][j]<<' ';
            cout<<endl;
        }
        cout<<endl;
    }
*/
    double ***type= new double**[m];
    for (int i=0;i<m;i++)
    {
        type[i]=new double*[t];
        for (int j=0;j<t;j++)
            type[i][j]=new double[n];
    }
    for (int i=0;i<m;i++)
        for (int j=0;j<t;j++)
        {
            distribution[i][j]=1.0;
            int jj = j;
            for (int k=0;k<n;k++)
            {
                type[i][j][k]=valuation[i][k][jj % tt];
                distribution[i][j] *= PL[jj % tt];
                jj/=tt;
            }
        }
    ans=0;
    for (int k=0;k<n;k++)
    {
        for (int i=0;i<m;i++)
            for (int j=0;j<tt;j++)
                if (valuation[i][k][0]>valuation[i][k][1])
                {
                    bid[i][j]=valuation[i][k][tt-1-j];
                    dis[i][j]=PL[tt-1-j];
                }
                else
                {
                    bid[i][j]=valuation[i][k][j];
                    dis[i][j]=1.0 / tt;
                }
        ans+=Myerson();
    }
    cout<<ans<<endl;
/*    for (int i=0;i<m;i++)
    {
        for (int j=0;j<t;j++)
        {
            for (int k=0;k<n;k++)
                cout<<type[i][j][k]<<' ';
            cout<<endl;
        }
        cout<<endl;
    }*/
    return 0;
}

