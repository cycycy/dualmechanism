#include "DualMechanism.h"

#include <cstdio>
#include <map>
#include <algorithm>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

const int ITEM_NUMBER = 2;
const int BIDDER_NUMBER = 4;
const int TYPE_NUMBER = 3; 
int valuation[BIDDER_NUMBER][ITEM_NUMBER][TYPE_NUMBER];

double ans = 0;
double maxi[ITEM_NUMBER];

int main()
{
    int n=ITEM_NUMBER,m=BIDDER_NUMBER,tt=TYPE_NUMBER;
    int t=1;
    for (int i=0;i<n;i++)
        t*=tt;
    cout<<t<<endl;
    double **distribution= new double*[m];
    for (int i=0;i<m;i++)
        distribution[i]=new double[t];
    for (int i=0;i<m;i++)
        for (int j=0;j<t;j++)
            distribution[i][j]=1.0/t;
    for (int i=0;i<m;i++)
        for (int k=0; k<n; k++)
            for (int j=0; j<tt;j++)
                scanf("%d ", &valuation[i][k][j]);
/*    for (int i=0;i<m;i++)
    {
        for (int k=0;k<n;k++)
        {
            for (int j=0;j<tt;j++)
                cout<<valuation[i][k][j]<<' ';
            cout<<endl;
        }
        cout<<endl;
    }
*/
    double ***type= new double**[m];
    for (int i=0;i<m;i++)
    {
        type[i]=new double*[t];
        for (int j=0;j<t;j++)
            type[i][j]=new double[n];
    }
    for (int i=0;i<m;i++)
        for (int j=0;j<t;j++)
        {
            int jj = j;
            for (int k=0;k<n;k++)
            {
                type[i][j][k]=valuation[i][k][jj % tt];
                jj/=tt;
            }
        }
/*    for (int i=0;i<m;i++)
    {
        for (int j=0;j<t;j++)
        {
            for (int k=0;k<n;k++)
                cout<<type[i][j][k]<<' ';
            cout<<endl;
        }
        cout<<endl;
    }*/
    int index[BIDDER_NUMBER];
    for (int i=0;i<m;i++)
        index[i]=0;
    while (index[0]<t)
    {
        double vcg = 0;
        for (int l=0;l<m;l++)
        {
            for (int k=0;k<n;k++)
                maxi[k]=0;
            for (int i=0;i<m;i++)
                if (i != l)
                    for (int k=0; k<n; k++)
                        if (type[i][index[i]][k] > maxi[k] + 1e-5)
                            maxi[k] = type[i][index[i]][k];
            for (int k=0;k<n;k++)
                vcg += maxi[k];
        }
        for (int k=0;k<n;k++)
            maxi[k]=0;
        for (int i=0;i<m;i++)
            for (int k=0;k<n;k++)
                if (type[i][index[i]][k] > maxi[k] + 1e-5)
                    maxi[k] = type[i][index[i]][k];
        for (int k=0;k<n;k++)
            vcg -= maxi[k] * (m-1);
        for (int i=0;i<m;i++)
            vcg*=distribution[i][index[i]];
        ans += vcg;
        int j=m-1;
        index[j]++;
        while (index[j]==t && j>0)
        {
            index[j]=0;
            index[j-1]++;
            j--;
        }
    }
    cout<<ans<<endl;
    DualMechanism mech(m,t,n);
    mech.SetType(type);
    mech.SetDistribution(distribution);
    mech.Solve(1e6,1e-3);
    cout<<mech.GetExpectedRevenue()<<endl;
    return 0;
}

