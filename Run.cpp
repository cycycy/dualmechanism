#include "DualMechanism.h"

#include <cstdio>
#include <map>
#include <algorithm>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

const int ITEM_NUMBER = 2;
const int BIDDER_NUMBER = 4;
const int TYPE_NUMBER = 11; int valuation[BIDDER_NUMBER][TYPE_NUMBER];
const double Gaussian[7] = {0.0045,0.0540,0.2420,0.3990,0.2420,0.0540,0.0045};
const double Exp[11] = {0.3334,0.2389,0.1711,0.1226,0.0879,0.0630,0.0451,0.0323,0.0232,0.0166,0.0119};
const double Beta[8] = {0.0028,0.0210,0.0743,0.1640,0.2506,0.2667,0.1760,0.0446};

int click_rate[ITEM_NUMBER];
int v[BIDDER_NUMBER],pmax[BIDDER_NUMBER],pmin[BIDDER_NUMBER];
double maxNash=0,minNash=0;
double maxans=0,minans=0;
void GSP()
{
    int n=ITEM_NUMBER,m=BIDDER_NUMBER,t=TYPE_NUMBER;
    sort(v,v+m);
    pmax[0]=v[m-n]*click_rate[0];
    pmin[0]=v[m-n-1]*click_rate[0];
    maxNash+=pmax[0];
    minNash+=pmin[0];
    for (int i=1;i<n;i++)
    {
        pmax[i]=pmax[i-1]+v[m-n+i]*(click_rate[i]-click_rate[i-1]);
        pmin[i]=pmin[i-1]+v[m-n+i-1]*(click_rate[i]-click_rate[i-1]);
        maxNash+=pmax[i];
        minNash+=pmin[i];
    }
}

int main()
{
    int n=ITEM_NUMBER,m=BIDDER_NUMBER,t=TYPE_NUMBER;
    double **distribution= new double*[m];
    for (int i=0;i<m;i++)
        distribution[i]=new double[t];
    for (int i=0;i<m;i++)
        for (int j=0;j<t;j++)
            distribution[i][j]=1.0/t;
    for (int i=0;i<m;i++)
    {
        int l,k;
        scanf("%d %d",&l,&k);
        for (int j=0;j<t;j++)
            valuation[i][j]=l+j*k;
    }
    for (int k=0;k<n;k++)
        scanf("%d",&click_rate[k]);
    int index[BIDDER_NUMBER];
    for (int i=0;i<m;i++)
        index[i]=0;
    while (index[0]<t)
    {
        maxNash=0;
        minNash=0;
        for (int i=0;i<m;i++)
            v[i]=valuation[i][index[i]];
        GSP();
        for (int i=0;i<m;i++)
        {
            maxNash*=distribution[i][index[i]];
            minNash*=distribution[i][index[i]];
        }
        maxans+=maxNash;
        minans+=minNash;
        int j=m-1;
        index[j]++;
        while (index[j]==t && j>0)
        {
            index[j]=0;
            index[j-1]++;
            j--;
        }
    }
    cout<<minans<<endl<<maxans<<endl;
    double ***type= new double**[m];
    for (int i=0;i<m;i++)
    {
        type[i]=new double*[t];
        for (int j=0;j<t;j++)
            type[i][j]=new double[n];
    }
    for (int i=0;i<m;i++)
        for (int j=0;j<t;j++)
            for (int k=0;k<n;k++)
                type[i][j][k]=valuation[i][j]*click_rate[k];
    DualMechanism mech(m,t,n);
    mech.SetType(type);
    mech.SetDistribution(distribution);
    mech.Solve(1e7,1e-4);
    cout<<mech.GetExpectedRevenue()<<endl;
    return 0;
}

